﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestEcrie : MonoBehaviour {

	public Text monEntrée;

	public bool enigme1;
	public bool enigme2;
	public bool enigme3;
	public bool enigme4;
	public bool enigme5;
	public bool enigme6;
	public bool enigme7;

	[HideInInspector]
	public bool enigme1Bis;
	public bool enigme2Bis;
	public bool enigme3Bis;
	public bool enigme4Bis;
	public bool enigme5Bis;
	public bool enigme6Bis;
	public bool enigme7Bis;


	public void Save(){
		
		SaveLeadManager.SaveEnigme (this);

	}

	public void Load(){
		
		bool[] loadedEnigmes = SaveLeadManager.LoadEnigme ();
		enigme1 = loadedEnigmes[0];
		enigme2 = loadedEnigmes[1];
		enigme3 = loadedEnigmes[2];
		enigme4 = loadedEnigmes[3];
		enigme5 = loadedEnigmes[4];
		enigme6 = loadedEnigmes[5];
		enigme7 = loadedEnigmes[6];

		enigme1Bis = loadedEnigmes[7];
		enigme2Bis = loadedEnigmes[8];
		enigme3Bis = loadedEnigmes[9];
		enigme4Bis = loadedEnigmes[10];
		enigme5Bis = loadedEnigmes[11];
		enigme6Bis = loadedEnigmes[12];
		enigme7Bis = loadedEnigmes[13];



	}
	// Use this for initialization
	void Start () {
		enigme1 = false;
		enigme2 = false;
		enigme3 = false;
		enigme4 = false;
		enigme5 = false;
		enigme6 = false;
		enigme7 = false;

		enigme1Bis = true;
		enigme2Bis = false;
		enigme3Bis = false;
		enigme4Bis = false;
		enigme5Bis = false;
		enigme6Bis = false;
		enigme7Bis = false;
	}
	
	// Update is called once per frame
	void Update () {


		if (monEntrée.text == "Salut" && enigme1Bis == true) {
			enigme1 = true;
			enigme1Bis = false;
			enigme2Bis = true;
		}
		if (monEntrée.text == "Salut2" && enigme2Bis == true) {
			enigme2 = true;
			enigme2Bis = false;
			enigme3Bis = true;
		}
		if (monEntrée.text == "Salut3" && enigme3Bis == true) {
			enigme3 = true;
			enigme3Bis = false;
			enigme4Bis = true;
		}
		if (monEntrée.text == "Salut4" && enigme4Bis == true) {
			enigme4 = true;
			enigme4Bis = false;
			enigme5Bis = true;
		}
		if (monEntrée.text == "Salut5" && enigme5Bis == true) {
			enigme5 = true;
			enigme5Bis = false;
			enigme6Bis = true;
		}
		if (monEntrée.text == "Salut6" && enigme6Bis == true) {
			enigme6 = true;
			enigme6Bis = false;
			enigme7Bis = true;
		}

		if (monEntrée.text == "Salut7" && enigme7Bis == true) {
			enigme7 = true;
			enigme7Bis = false;
		}
	}
}
