﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPS : MonoBehaviour {

	public static GPS Instance { set; get;}

	public float latitude;
	public float longitude;

	public float latiStart;
	public float longiStart;

	// Use this for initialization
	void Start () {
		Instance = this;
		DontDestroyOnLoad (gameObject);
		StartCoroutine ("LePetitGPS");
		latiStart = Input.location.lastData.latitude;
		longiStart = Input.location.lastData.longitude;
	}
	
	// Update is called once per frame
	void Update () {
		Instance = this;
		latitude = Input.location.lastData.latitude;
		longitude = Input.location.lastData.longitude;
	}

	IEnumerator LePetitGPS()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			Debug.Log("Timed out");
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			Debug.Log("Unable to determine device location");
			yield break;
		}
	
			// Access granted and location value could be retrieved
			//Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

		latitude = Input.location.lastData.latitude;
		longitude = Input.location.lastData.longitude;

		// Stop service if there is no need to query location updates continuously
		//Input.location.Stop();
	}
}
