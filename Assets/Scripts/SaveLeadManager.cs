﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLeadManager {

	public static void SaveEnigme(TestEcrie enigme){
		
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream (Application.persistentDataPath + "/enigme.sav", FileMode.Create);

		EnigmeData data = new EnigmeData (enigme);
	
		bf.Serialize (stream, data);
		stream.Close ();

	}
	public static bool[] LoadEnigme(){
	
		if (File.Exists (Application.persistentDataPath + "/enigme.sav")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream stream = new FileStream (Application.persistentDataPath + "/enigme.sav", FileMode.Open);

			EnigmeData data = bf.Deserialize (stream) as EnigmeData;
			stream.Close ();

			return data.stats;
		} else {
			Debug.LogError ("No save data");
			return new bool[14];
		}
	}

}

[Serializable]
public class EnigmeData{

	public bool[] stats;

	public EnigmeData ( TestEcrie enigme){
		stats = new bool[14];

		stats[0] = enigme.enigme1;
		stats[1] = enigme.enigme2;
		stats[2] = enigme.enigme3 ;
		stats[3] = enigme.enigme4 ;
		stats[4] = enigme.enigme5 ;
		stats[5] = enigme.enigme6 ;
		stats[6] = enigme.enigme7;

		stats[7] = enigme.enigme1Bis;
		stats[8] = enigme.enigme2Bis;
		stats[9] = enigme.enigme3Bis;
		stats[10] = enigme.enigme4Bis;
		stats[11] = enigme.enigme5Bis;
		stats[12] = enigme.enigme6Bis;
		stats[13] = enigme.enigme7Bis; 

	}

}