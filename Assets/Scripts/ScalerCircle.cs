﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScalerCircle : MonoBehaviour {

	public GameObject sliderZoom;

	public float leZoom;
	public float maxValuet;

	// Use this for initialization
	void Start () {
		leZoom = sliderZoom.GetComponent<Slider> ().value;
		maxValuet = sliderZoom.GetComponent<Slider> ().maxValue;
	}
	
	// Update is called once per frame
	void Update () {
		leZoom = sliderZoom.GetComponent<Slider> ().value;
		transform.localScale = new Vector3 ((maxValuet/leZoom), (maxValuet/leZoom), (maxValuet/leZoom));
	}
}
