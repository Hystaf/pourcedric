﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Googleapi : MonoBehaviour  {

	public RawImage img;
	public GameObject sliderZoom;

	string url;

	public float latMarker;
	public float lonMarker;

	public float testDistance;

	public int monNumber;

	public float lat;
	public float lon;

	LocationInfo li;

	public float zoom = 14;

	public int mapWidth =640;
	public int mapHeight = 640;

	public enum mapType {roadmap,satellite,hybrid,terrain}
	public mapType mapSelected;
	public int scale;


	IEnumerator Map()
	{
		while (true) {

			Resources.UnloadUnusedAssets ();
			//Debug.Log ("In Map1");
			url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon +
			"&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
			+ "&maptype=" + mapSelected +
				/*"&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318*/"&markers=color:red%7Clabel:"+ monNumber +"%7C"+latMarker+","+lonMarker+"&path=color:0x0000ff|weight:5|" + lat + "," + lon + "|" + lat + "," + lon + "|" + latMarker + "," + lonMarker + "|" + latMarker + "," + lonMarker + "&interpolate=false&key=AIzaSyCPURm9olznmtV-6DjdfbARVThZb2waxWY";

		
			//url = "https://maps.googleapis.com/maps/api/staticmap?center=45,5 &zoom=14 &size=640x640&scale=0&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyCPURm9olznmtV-6DjdfbARVThZb2waxWY";
			//Debug.Log ("In Map2");
			WWW www = new WWW (url);
			//Debug.Log ("In Map3");
			yield return www;
			//Debug.Log ("In Map4");
			img.texture = www.texture;
			//Debug.Log ("In Map5");
			img.SetNativeSize ();

			yield return new WaitForSeconds (0.5f);
			//Debug.Log ("In Map6");


		}
		//StartCoroutine (Map());
	}

	// Use this for initialization
	void Start () {
		monNumber = 0;
		latMarker = lat;
		lonMarker = lon;
		img = gameObject.GetComponent<RawImage> ();
		StartCoroutine (Map());
	}

	// Update is called once per frame
	void Update () {
		lat = GPS.Instance.latitude;
		lon = GPS.Instance.longitude;
		zoom = sliderZoom.GetComponent<Slider> ().value;

		testDistance = Calculate (lat, lon, latMarker, lonMarker);

		//StartCoroutine (Map());

	}

	public static float Calculate(float sLatitude,float sLongitude, float eLatitude, 
		float eLongitude)
	{
		float radiansOverDegrees = (Mathf.PI / 180.0f);

		float sLatitudeRadians = sLatitude * radiansOverDegrees;
		float sLongitudeRadians = sLongitude * radiansOverDegrees;
		float eLatitudeRadians = eLatitude * radiansOverDegrees;
		float eLongitudeRadians = eLongitude * radiansOverDegrees;

		float dLongitude = eLongitudeRadians - sLongitudeRadians;
		float dLatitude = eLatitudeRadians - sLatitudeRadians;

		float result1 = Mathf.Pow(Mathf.Sin(dLatitude / 2.0f), 2.0f) + 
			Mathf.Cos(sLatitudeRadians) * Mathf.Cos(eLatitudeRadians) * 
			Mathf.Pow(Mathf.Sin(dLongitude / 2.0f), 2.0f);

		// Using 3956 as the number of miles around the earth
		float result2 = 3956.0f * 2.0f * 1.60934f *
			Mathf.Atan2(Mathf.Sqrt(result1), Mathf.Sqrt(1.0f - result1));

		return result2;
	
	}
}
