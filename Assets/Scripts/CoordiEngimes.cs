﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoordiEngimes : MonoBehaviour {

	public GameObject enigmes;
	public GameObject imageRaw;

	[SerializeField]
	Vector2 empla1, empla2, empla3, empla4, empla5, empla6, empla7;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if (enigmes.GetComponent<TestEcrie> ().enigme1 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 1;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla1.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla1.y;
		}
		if (enigmes.GetComponent<TestEcrie> ().enigme2 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 2;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla2.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla2.y;
		}
		if (enigmes.GetComponent<TestEcrie> ().enigme3 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 3;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla3.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla3.y;
		}
		if (enigmes.GetComponent<TestEcrie> ().enigme4 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 4;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla4.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla4.y;
		}
		if (enigmes.GetComponent<TestEcrie> ().enigme5 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 5;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla5.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla5.y;
		}
		if (enigmes.GetComponent<TestEcrie> ().enigme6 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 6;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla6.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla6.y;
		}
		if (enigmes.GetComponent<TestEcrie> ().enigme7 == true) {
			imageRaw.GetComponent<Googleapi> ().monNumber = 7;
			imageRaw.GetComponent<Googleapi> ().latMarker = empla7.x;
			imageRaw.GetComponent<Googleapi> ().lonMarker = empla7.y;
		}

	}
}
