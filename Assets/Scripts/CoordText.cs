﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoordText : MonoBehaviour {

	public Text coordinate;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		coordinate.text = "Lat: " + GPS.Instance.latitude.ToString() + " Long: " + GPS.Instance.longitude.ToString();
	}
}
